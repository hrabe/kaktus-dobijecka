package com.example.kaktusdobijecka.crawler

import com.example.kaktusdobijecka.model.CactusRecharger
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
//import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.*

// mock final classes
// https://github.com/mockito/mockito/wiki/What%27s-new-in-Mockito-2#mock-the-unmockable-opt-in-mocking-of-final-classesmethods
@RunWith(MockitoJUnitRunner::class)
class CactusRechargerParserUnitTest {

    @Test
    fun parse_emptyMessage_isEmptyList() {
        val emptyMessage = ""

        val mockCactusRechargerDownloader = mock<CactusRechargerDownloader> {
            on { getCactusRechargerMessage() } doReturn listOf(emptyMessage)
        }
        val testObject = CactusRechargerParser(mockCactusRechargerDownloader)
        val result = testObject.parseAll()
        Assert.assertEquals(result, emptyList<CactusRecharger>())

        verify(mockCactusRechargerDownloader, times(1)).getCactusRechargerMessage()
    }

    @Test
    fun parse_validMessages_isCorrectlyParsed() {
        val messages = listOf(
            "Zasaď dneska 20. 4. 200 - 500 kaček mezi 18 a 21 hodinou a nech svůj kredit" +
                    " \uD83C\uDF31 rozkvést do dvojnásobný velikosti.",
            "Stačí dnes 11. 4. dobít mezi 17 a 20 hodinou 200 - 500 kaček a" +
                    " tvá porce volání, sms a dat poroste jak 🍄 po dešti.",
            "Když pořídíš kredit za 2 až 5 stovek dneska 21. 6. mezi " +
                "17. a 19. hodinou, vyznamenáme tě 2x takovym kreditem. \uD83C\uDF35",
            "Ukážeme ti pořádný kouzla ⚡️ Stačí, když si dneska 11. 1. od 16 do 19 hodin dobiješ " +
                    "za 2 až 5 stovek a objeví se ti dvakrát takovej kredit. Kredity na " +
                    "tebe! \uD83C\uDF35 Sdílet na Facebooku",
            "Dobij si dneska 9. 6. od 17:00 do 20:00 "+
                    "mezi 2 - 5 kilama a smlsni si na dvojnásobnym kreditu. \uD83E\uDD60",
            "Postačí, když si dneska 26. 10. dobiješ mezi 18. a 20. hodinou za 2 až 5 kil, tak" +
                    " datování, zprávám i volání můžeš dát pěkně do těla. \uD83C\uDF35 Sdílet " +
                    "na Facebooku"
        )

        val mockCactusRechargerDownloader = mock<CactusRechargerDownloader> {
            on { getCactusRechargerMessage() } doReturn messages
        }
        val testObject = CactusRechargerParser(mockCactusRechargerDownloader)
        val result = testObject.parseAll()

        Assert.assertEquals(result.size, messages.size)

        result.forEachIndexed {
            index, p -> Assert.assertEquals(p.message, messages[index])
        }
    }
}