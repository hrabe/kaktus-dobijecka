package com.example.kaktusdobijecka

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.kaktusdobijecka.crawler.CactusRechargerParser
import com.example.kaktusdobijecka.model.CactusRecharger
import com.example.kaktusdobijecka.room.CacheEntityMapper
import com.example.kaktusdobijecka.room.CactusRechargerDao
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

@HiltWorker
class NotificationWorker
    @AssistedInject
    constructor(@Assisted appContext: Context,
                @Assisted workerParams: WorkerParameters,
                private val cactusRechargerDao: CactusRechargerDao,
                private val cactusRechargerParser: CactusRechargerParser,
                private val cactusRechargerCacheEntityMapper: CacheEntityMapper
    ) : CoroutineWorker(appContext, workerParams) {

    // suspend can be run only in coroutine
    override suspend fun doWork(): Result {
        Log.i("NotificationWorker", "Start work")
        val cactusRechargerList = cactusRechargerParser.parseAll()

        if(cactusRechargerList.isNotEmpty()){
            val latestCactusRecharger = cactusRechargerList.first()
            cactusRechargerDao.insert(
                cactusRechargerCacheEntityMapper.mapToEntity(latestCactusRecharger)
            )
            if ( shouldFireNotification(latestCactusRecharger) ){
                Log.i("NotificationWorker", "DoTheNotification")
                val title = applicationContext.getString(R.string.recharger_notification_title)
                makeNotification(latestCactusRecharger.message, title)
            }else{
                makeNotification(latestCactusRecharger.message, "Nic [debug only]")
            }
        }

        Log.i("NotificationWorker", "End work")
        return Result.success()
    }

    private fun shouldFireNotification(cactusRecharger: CactusRecharger) : Boolean {
        val localNow = LocalDateTime.now()
        val zonedUTC: ZonedDateTime = localNow.atZone(ZoneId.of("UTC"))

        return cactusRecharger.startDateUtc < zonedUTC && zonedUTC < cactusRecharger.endDateUtc
    }

    private fun makeNotification(message: String, title: String) {
        val notificationManager: NotificationManager = applicationContext.
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel =
                NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    "notificationWorker",
                    NotificationManager.IMPORTANCE_DEFAULT
                )
            notificationManager.createNotificationChannel(notificationChannel)
        }

        val notificationBuilder = NotificationCompat.Builder(applicationContext,
            NOTIFICATION_CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(message)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setStyle(
                NotificationCompat.BigTextStyle()
                .bigText(message))
            .build()
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder)
    }

    companion object {
        private const val NOTIFICATION_ID = 156646
        private const val NOTIFICATION_CHANNEL_ID = "cactus_recharger_01"
    }

}