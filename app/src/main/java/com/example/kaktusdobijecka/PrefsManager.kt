package com.example.kaktusdobijecka

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.preferencesDataStore
import com.example.kaktusdobijecka.PrefsManager.Companion.DATA_STORE_NAME
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

private val Context.dataStore: DataStore<Preferences>
        by preferencesDataStore(name = DATA_STORE_NAME)

class PrefsManager
@Inject
constructor(
    private val context: Context
){

    suspend fun saveMessage(value: Set<String>) {
        context.dataStore.edit {
            preferences ->
            preferences[NABIJECKA_MESSAGE] = value
        }
    }

    fun getMessage(): Flow<Set<Any>> =
        context.dataStore.data.map {
            it[NABIJECKA_MESSAGE] ?: setOf(String)
        }

    companion object {
        const val DATA_STORE_NAME = "data_store"

        val NABIJECKA_MESSAGE = stringSetPreferencesKey("kaktus_nabijecka_notification")
    }
}