package com.example.kaktusdobijecka.di

import com.example.kaktusdobijecka.crawler.CactusRechargerDownloader
import com.example.kaktusdobijecka.crawler.CactusRechargerParser
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class CrawlerModule {

    @Singleton
    @Provides
    fun provideCactusRechargerDownloader(): CactusRechargerDownloader{
        return CactusRechargerDownloader()
    }

    @Singleton
    @Provides
    fun provideCactusRecharger(
        cactusRechargerDownloader: CactusRechargerDownloader
    ) : CactusRechargerParser {
        return CactusRechargerParser(cactusRechargerDownloader)
    }
}