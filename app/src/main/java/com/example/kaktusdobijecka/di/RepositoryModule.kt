package com.example.kaktusdobijecka.di

import com.example.kaktusdobijecka.crawler.CactusRechargerParser
import com.example.kaktusdobijecka.repository.CactusRechargerRepository
import com.example.kaktusdobijecka.room.CacheEntityMapper
import com.example.kaktusdobijecka.room.CactusRechargerDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun providesCactusRechargerRepository(
        cactusRechargerDao: CactusRechargerDao,
        cactusRechargerParser: CactusRechargerParser,
        cactusRechargerCacheEntityMapper: CacheEntityMapper
    ) : CactusRechargerRepository{
        return CactusRechargerRepository(
            cactusRechargerDao, cactusRechargerParser, cactusRechargerCacheEntityMapper
        )
    }
}