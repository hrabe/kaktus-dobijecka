package com.example.kaktusdobijecka.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.example.kaktusdobijecka.room.CactusRechargerDao
import com.example.kaktusdobijecka.room.CactusRechargerDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RoomModule {

    @Singleton
    @Provides
    fun provideCactusRechargerDatabase(@ApplicationContext context: Context) :
            CactusRechargerDatabase{
        return Room.databaseBuilder(
            context,
            CactusRechargerDatabase::class.java,
            CactusRechargerDatabase.DATABASE_NAME
        ).fallbackToDestructiveMigration().build()
    }

    @Singleton
    @Provides
    fun provideCactusRechargerDao(cactusRechargerDatabase: CactusRechargerDatabase):
            CactusRechargerDao{
        return cactusRechargerDatabase.cactusRechargerDao()
    }
}