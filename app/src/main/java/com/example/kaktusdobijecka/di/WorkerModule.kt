package com.example.kaktusdobijecka.di

import android.content.Context
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import androidx.work.WorkRequest
import com.example.kaktusdobijecka.NotificationWorker
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class WorkerModule {
//    @Singleton
//    @Provides
//    fun provideWorkerManager (
//        @ApplicationContext
//        context: Context
//    ): WorkManager {
//        return WorkManager.getInstance(context)
//    }
//
//    @Singleton
//    @Provides
//    fun provideWorkRequest () : WorkRequest {
//        return PeriodicWorkRequest.Builder(
//            NotificationWorker::class.java,
//            30, TimeUnit.MINUTES)
//            .addTag("notificationWorker")
//            .build()
//    }
}