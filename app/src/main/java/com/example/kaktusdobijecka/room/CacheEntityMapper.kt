package com.example.kaktusdobijecka.room

import com.example.kaktusdobijecka.model.CactusRecharger
import javax.inject.Inject

class CacheEntityMapper
@Inject constructor() : EntityMapper<CactusRechargerCacheEntity, CactusRecharger>{
    override fun mapFromEntity(entity: CactusRechargerCacheEntity): CactusRecharger {
         return CactusRecharger(
             message = entity.message,
             startDateUtc = entity.startDateUtc,
             endDateUtc = entity.endDateUtc,
             timeCreatedUtc = entity.timeCreatedUtc
         )
    }

    override fun mapToEntity(domainModel: CactusRecharger): CactusRechargerCacheEntity {
        return CactusRechargerCacheEntity(
            message = domainModel.message,
            startDateUtc = domainModel.startDateUtc,
            endDateUtc = domainModel.endDateUtc,
            timeCreatedUtc = domainModel.timeCreatedUtc
        )
    }

    fun mapFromEntityList(entities: List<CactusRechargerCacheEntity>): List<CactusRecharger> {
        return entities.map { mapFromEntity(it) }
    }

    fun mapToEntityList(entities: List<CactusRecharger>): List<CactusRechargerCacheEntity> {
        return entities.map { mapToEntity(it) }
    }
}