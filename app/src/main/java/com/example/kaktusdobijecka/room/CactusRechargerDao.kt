package com.example.kaktusdobijecka.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CactusRechargerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(CactusRechargerEntity: CactusRechargerCacheEntity): Long

    @Query("SELECT * FROM events")
    suspend fun get(): List<CactusRechargerCacheEntity>
}