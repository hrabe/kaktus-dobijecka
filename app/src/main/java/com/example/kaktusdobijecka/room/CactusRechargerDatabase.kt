package com.example.kaktusdobijecka.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.example.kaktusdobijecka.model.CactusRecharger
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime

@Database(entities = [CactusRechargerCacheEntity::class], version = 2)
@TypeConverters(Converters::class)
abstract class CactusRechargerDatabase: RoomDatabase() {
    abstract fun cactusRechargerDao(): CactusRechargerDao

    companion object {
        const val DATABASE_NAME: String = "recharger_db"
    }
}
