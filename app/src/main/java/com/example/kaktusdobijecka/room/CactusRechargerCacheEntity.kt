package com.example.kaktusdobijecka.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.temporal.TemporalQueries.zoneId

@Entity(tableName = "events")
data class CactusRechargerCacheEntity (
    // @PrimaryKey(autoGenerate = true)
    // @ColumnInfo(name = "id")
    // TODO: check if auto-generates correctly
    // val id: Int = 0,

    @ColumnInfo(name = "message")
    var message: String,

    @ColumnInfo(name = "timeCreatedUtc")
    var timeCreatedUtc: ZonedDateTime,

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "startDateUtc")
    var startDateUtc: ZonedDateTime,

    @ColumnInfo(name = "endDateUtc")
    var endDateUtc: ZonedDateTime,
)

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long): ZonedDateTime {
        val i: Instant = Instant.ofEpochSecond(value)
        return value.let { ZonedDateTime.ofInstant(i, ZoneId.of("UTC")) }
    }

    @TypeConverter
    fun dateToTimestamp(date: ZonedDateTime): Long {
        return date.toEpochSecond()
    }
}
