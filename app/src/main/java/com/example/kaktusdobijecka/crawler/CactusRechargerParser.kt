package com.example.kaktusdobijecka.crawler

import com.example.kaktusdobijecka.model.CactusRecharger
import kotlinx.coroutines.yield
import java.text.ParseException
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import javax.inject.Inject

class CactusRechargerParser {
    private val cactusRechargerDownloader: CactusRechargerDownloader

    @Inject
    constructor(cactusRechargerDownloader: CactusRechargerDownloader){
        this.cactusRechargerDownloader = cactusRechargerDownloader
    }

    fun parseAll(): List<CactusRecharger> {
        val messages = cactusRechargerDownloader.getCactusRechargerMessage()
        return messages.mapNotNull { p -> parse(p) }
    }

    private fun findDate(message: String): MatchResult? {
        return Regex("(\\d+)\\.\\s*(\\d+)\\.").find(message)
    }

    private fun findRange(message: String): MatchResult?{
        var rangeMatch = Regex("(\\d+)\\.\\s*a\\s*(\\d+)\\.").find(message)

        if (rangeMatch != null) return rangeMatch
        rangeMatch = Regex("(\\d+):\\d+.+(\\d+):\\d+").find(message)

        if (rangeMatch != null) return rangeMatch
        rangeMatch = Regex("od.+(\\d+).+do.+(\\d+)").find(message)

        if (rangeMatch != null) return rangeMatch
        rangeMatch = Regex("mezi.+(\\d+)\\.?.+a.+(\\d+)\\.?").find(message)

        return rangeMatch
    }

    private fun parse(message: String): CactusRecharger? {
        var dateMatch: MatchResult?
        var rangeMatch: MatchResult?
        var cactusRecharger: CactusRecharger? = null

        try {
            dateMatch = findDate(message)
            rangeMatch = findRange(message)


            val (day, month) = dateMatch?.destructured!!
            val (startHour, endHour) = rangeMatch?.destructured!!

            if(day == null || month == null || startHour == null || endHour == null ){
                throw ParseException("day: $day, month: $month, start hour: $startHour and " +
                        "end hour : $endHour could not parsed", 0)
            }

            val localNow = LocalDateTime.now()
            val zonedDateTime: ZonedDateTime = localNow.atZone(ZoneId.systemDefault())
            val zonedPrague = zonedDateTime.withZoneSameInstant(ZoneId.of("Europe/Prague"))

            val startDateUtc = zonedPrague
                .withDayOfMonth(day.toInt())
                .withMonth(month.toInt())
                .withHour(startHour.toInt())
                .withMinute(0)
                .withSecond(0).withZoneSameInstant(ZoneId.of("UTC"))
            val endDateUtc = startDateUtc
                .withHour(endHour.toInt()).withZoneSameInstant(ZoneId.of("UTC"))

            cactusRecharger = CactusRecharger(localNow.atZone(ZoneId.of("UTC")),
                startDateUtc, endDateUtc, message)
        }catch (e: Exception){
            System.err.println(message)
            e.printStackTrace()
        }

        return cactusRecharger
    }
}