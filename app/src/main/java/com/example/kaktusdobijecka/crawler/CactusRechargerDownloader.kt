package com.example.kaktusdobijecka.crawler

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jsoup.Jsoup
import javax.inject.Inject

class CactusRechargerDownloader @Inject constructor(){
    private val cactusRechargerUrl = "https://mujkaktus.cz/chces-pridat"

    fun getCactusRechargerMessage() : List<String> {
        var announceText = listOf<String>()
        try {
                val doc = Jsoup.connect(cactusRechargerUrl).get()
                val bubbleElement = doc.getElementsByClass("box-bubble").first()
                val articleElements = bubbleElement!!
                    .getElementsByClass("journal-content-article")
                val announceElement = articleElements.map {
                    it.getElementsByTag("p").text()
                }.filter { it.isNotEmpty() }

                announceText = announceElement
        }
        catch (e: Exception)  {
            e.printStackTrace()
        }
        return announceText
    }

    companion object
}