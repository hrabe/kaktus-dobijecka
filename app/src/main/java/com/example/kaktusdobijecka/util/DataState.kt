package com.example.kaktusdobijecka.util

import java.lang.Exception

/*
 Not standard DataState class, simplified version
 https://www.youtube.com/watch?v=8vAQrgbh6YM&list=PLgCYzUzKIBE_MUlyvbCiOWsfq0nFgGXQ9&index=11
 */
sealed class DataState<out R> {
    data class Success<out T>(val data: T): DataState<T>()
    data class Error(val exception: Exception): DataState<Nothing>()
    object Loading : DataState<Nothing>()
}