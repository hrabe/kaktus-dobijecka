package com.example.kaktusdobijecka.gui

import androidx.lifecycle.*
import com.example.kaktusdobijecka.model.CactusRecharger
import com.example.kaktusdobijecka.repository.CactusRechargerRepository
import com.example.kaktusdobijecka.util.DataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val cactusRechargerRepository: CactusRechargerRepository
): ViewModel(){
    private val _dataState: MutableLiveData<DataState<List<CactusRecharger>>> = MutableLiveData()

    val dataState: LiveData<DataState<List<CactusRecharger>>>
        get() = _dataState

    fun setStateEvent(mainStateEvent: MainStateEvent){
        viewModelScope.launch {
            when(mainStateEvent){
                is MainStateEvent.GetCactusRechargerEvent -> {
                    cactusRechargerRepository.getCactusRechargerList().onEach {
                        dataState -> _dataState.value = dataState
                    }.launchIn(viewModelScope)
                }

                is MainStateEvent.None -> {
                    // not implemented
                }
            }
        }
    }
}

sealed class MainStateEvent {
    object GetCactusRechargerEvent: MainStateEvent()

    object None: MainStateEvent()
}