package com.example.kaktusdobijecka

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.annotation.StringRes
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.map
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequest
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.example.kaktusdobijecka.gui.MainStateEvent
import com.example.kaktusdobijecka.gui.MainViewModel
import com.example.kaktusdobijecka.model.CactusRecharger
import com.example.kaktusdobijecka.ui.theme.DefaultTheme
import com.example.kaktusdobijecka.util.DataState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.lang.StringBuilder
import java.time.ZonedDateTime
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mainViewModel.setStateEvent(MainStateEvent.GetCactusRechargerEvent)
        setContent {
            val data by mainViewModel.dataState.observeAsState()

            DefaultTheme {
                MainScreen(data = data)
            }
        }
        notificationWorker()
    }

    private fun notificationWorker() {
        val request = PeriodicWorkRequest.Builder(
            NotificationWorker::class.java,
            30, TimeUnit.MINUTES)
            .addTag("notificationWorker")
            .build()
        WorkManager.getInstance(applicationContext).enqueueUniquePeriodicWork(
            "myPW", ExistingPeriodicWorkPolicy.REPLACE,
            request)
    }
    private fun displayError(message: String?){
//        if(message != null){
//            text.text = message
//        }
//        else{
//            text.text = "Unknown error"
//        }
    }

    private fun displayProgressBar(isDisplayed: Boolean){
//        progress_bar.visibility = if(isDisplayed) View.VISIBLE else View.GONE
    }

    private fun appendCactusNotification(cactusRechargerList: List<CactusRecharger>){
        val sb = StringBuilder()
        for(cactusRecharger in cactusRechargerList){
            sb.append(cactusRecharger.message + "\n")
        }
//        text.text = sb.toString()
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview(){
    DefaultTheme {
        MainScreen(
            DataState.Success(
                listOf(
                    CactusRecharger(
                        ZonedDateTime.now(),
                        ZonedDateTime.now(),
                        ZonedDateTime.now(),
                        "Když pořídíš kredit za 2 až 5 stovek dneska 21. 6. mezi 17. a 19. hodinou, vyznamenáme tě 2x takovym kreditem. \uD83C\uDF35"
                    ),
                    CactusRecharger(
                        ZonedDateTime.now(),
                        ZonedDateTime.now(),
                        ZonedDateTime.now(),
                        "Dobij si dneska 9. 6. od 17:00 do 20:00 mezi 2 - 5 kilama a smlsni si na dvojnásobnym kreditu. \uD83E\uDD60"
                    )
                )
            )
        )
    }
}

// Creating a function to get string id
@Composable
@ReadOnlyComposable
fun textResource(@StringRes id: Int): CharSequence =
    LocalContext.current.resources.getText(id)

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MainScreen(data: DataState<List<CactusRecharger>>?){
    val padding = 16.dp
    Column(
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .background(color = MaterialTheme.colors.background)
            .fillMaxHeight()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier
                .weight(2F)
        ) {
            Text(
                text = stringResource(R.string.main_screen_title),
                color = MaterialTheme.colors.secondaryVariant,
                style = MaterialTheme.typography.h3,
                textAlign = TextAlign.Center,
                letterSpacing = 6.sp
            )
        }
//            Spacer(Modifier.size(padding))
        Row(
            verticalAlignment = Alignment.CenterVertically,
//            horizontalArrangement = Arrangement.SpaceAround,
            modifier = Modifier
                    .weight(1F)
        ) {
            Column(
                modifier = Modifier
                    .height(intrinsicSize = IntrinsicSize.Max)
                    .fillMaxWidth(fraction = 0.5f)
            ){
                SwitchNotificationFunctionality()
            }

            Column(
                Modifier.fillMaxWidth(fraction = 0.5f)
            ) {
//                Spacer(modifier = Modifier.padding(padding))
            }

        }
        Row(
            verticalAlignment = Alignment.Top,
            modifier = Modifier
                .weight(2F)
        ) {
            ShowLatestNotification(data = data)
//            rectangleShape()
        }
    }
}

//@Composable
//fun rectangleShape(){
//    shapeBackground(shape = RoundedCornerShape(10.dp))
//}
//@Composable
//fun shapeBackground(shape: Shape){
//    Column(modifier = Modifier
//        .fillMaxWidth()
//        .wrapContentSize(Alignment.Center)) {
//        Box(
//            modifier = Modifier
//                .size(300.dp)
//                .clip(shape)
//                .background(Color.Red)
//        )
//    }
//}

@Composable
fun ShowLatestNotification(data: DataState<List<CactusRecharger>>?) {
    Column (
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .padding(all = 20.dp)
            .fillMaxHeight()
    ){
        when(data){
            is DataState.Success<List<CactusRecharger>> -> {
                DataBox(message = data.data.minByOrNull { x -> x.startDateUtc }!!.message)
            }
            is DataState.Error -> {
                DataBox(message = data.exception)
            }
            is DataState.Loading -> {
                CircularProgressIndicator()
            }
        }
    }
}

@Composable
fun DataBox(message: Any) {
    Row(horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.Top,
        modifier = Modifier
            .padding(3.dp, 2.dp)
    ) {
        Text(
            color = MaterialTheme.colors.secondaryVariant,
            style = MaterialTheme.typography.h6,
            textAlign = TextAlign.Left,
            text = message.toString(),
            modifier = Modifier.graphicsLayer {
                shadowElevation = 8.dp.toPx()
                shape = CutCornerShape(32.dp)
                clip = true
            }.background(color = MaterialTheme.colors.surface)
                .fillMaxHeight()
                .padding(20.dp)
        )
    }
}

@Composable
fun SwitchNotificationFunctionality() {

    val mCheckedState = remember { mutableStateOf(true) }
    Row (
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier
            .fillMaxWidth()
    )
    {
        Switch(
            checked = mCheckedState.value,
            onCheckedChange = { mCheckedState.value = it },
            modifier = Modifier.scale(2f)
        )
    }
    Row (
        modifier = Modifier
            .fillMaxWidth()
    )
    {
        val text = if (mCheckedState.value)
            stringResource(id = R.string.main_screen_notification_enabled) else
            stringResource(id = R.string.main_screen_notification_disabled)
        Text(
            color = MaterialTheme.colors.secondaryVariant,
            text = text)
    }
}