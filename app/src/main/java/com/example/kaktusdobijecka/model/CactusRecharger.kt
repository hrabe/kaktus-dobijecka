package com.example.kaktusdobijecka.model

import java.time.ZonedDateTime

data class CactusRecharger(
    var timeCreatedUtc: ZonedDateTime,
    var startDateUtc: ZonedDateTime,
    var endDateUtc: ZonedDateTime,
    var message: String
)
