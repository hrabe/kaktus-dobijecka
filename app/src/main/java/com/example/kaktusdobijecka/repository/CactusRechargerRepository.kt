package com.example.kaktusdobijecka.repository

import android.content.Context
import com.example.kaktusdobijecka.crawler.CactusRechargerParser
import com.example.kaktusdobijecka.model.CactusRecharger
import com.example.kaktusdobijecka.room.CacheEntityMapper
import com.example.kaktusdobijecka.room.CactusRechargerDao
import com.example.kaktusdobijecka.util.DataState
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class CactusRechargerRepository
constructor(
    private val cactusRechargerDao: CactusRechargerDao,
    private val cactusRechargerParser: CactusRechargerParser,
    private val cactusRechargerCacheEntityMapper: CacheEntityMapper
){
    suspend fun getCactusRechargerList() : Flow<DataState<List<CactusRecharger>>> = flow {
        emit(DataState.Loading)
        // only for development purposes, delete me in PROD
        delay(5000)
        try {
//            coroutineScope {
//                val cactusRechargerList = cactusRechargerParser.parseAll()
//                for (cactusRecharger in cactusRechargerList) {
//                    cactusRechargerDao.insert(
//                        cactusRechargerCacheEntityMapper.mapToEntity(cactusRecharger)
//                    )
//                }
//            }

            val cachedCactusRechargerList = cactusRechargerDao.get()
            emit(DataState.Success(
                cactusRechargerCacheEntityMapper.mapFromEntityList(cachedCactusRechargerList)
            ))
        }catch (e: Exception){
            emit(DataState.Error(e))
        }
    }
}